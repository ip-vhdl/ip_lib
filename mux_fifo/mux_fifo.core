CAPI=2:

name : ::mux_fifo:0

filesets:

  core.rtl:
    files:
      - src/ip/mux_fifo.vhd
    file_type: vhdlSource-2008
    depend : [edacom]

  implement.rtl:
    files:
      - src/impl/top.vhd
    file_type: vhdlSource-2008

  simulate.rtl:
    files:
      - sim/runs/mux_fifo.cocotb/hdl/mux_fifo_tb.vhd
      - ip/mux_fifo_fifo/mux_fifo_fifo_sim_netlist.vhdl
      - ip/mux_fifo_fifo_out/mux_fifo_fifo_out_sim_netlist.vhdl
    file_type: vhdlSource-2008

  simulate.extras:
    files:
      - sim/runs/mux_fifo.cocotb/wave.gtkw : {copyto : .}
      - sim/runs/mux_fifo.cocotb/Makefile.mux_fifo : {copyto : .}
      - sim/runs/mux_fifo.cocotb/mux_fifo_tb.py : {copyto : .}
      - sim/runs/mux_fifo.cocotb/tests/test.osvvm.simple.sh : {copyto : .}
      - sim/runs/mux_fifo.cocotb/tests/test.osvvm.complex.sh : {copyto : .}
      - sim/runs/mux_fifo.cocotb/tests/test.uniform.simple.sh : {copyto : .}
      - sim/runs/mux_fifo.cocotb/tests/test.uniform.complex.sh : {copyto : .}
    file_type: user

# * Targets

targets:

  default:
    description : target to be use when ip is used as a dependency
    filesets: [core.rtl]

  # ** Implement

  implement:
    default_tool : vivado
    description : implement example use design with xilinx vivado
    filesets : [core.rtl, implement.rtl]
    tools:
      vivado:
        part : xc7k325t
    toplevel : top

  # ** Simulate

  # *** Test simple with random.uniform

  simulate.run.uniform.simple:
    default_tool: generic
    filesets : [core.rtl, simulate.rtl, simulate.extras]
    description : simulate with ghdl using own project makefile
    tools:
      generic:
        # build executable
        build_cmd: chmod +x test*.sh
        # run simulation
        run_cmd: ./test.uniform.simple.sh
    toplevel: mux_fifo_tb

  # *** Test complex with random.uniform

  simulate.run.uniform.complex:
    default_tool: generic
    filesets : [core.rtl, simulate.rtl, simulate.extras]
    description : simulate with ghdl using own project makefile
    tools:
      generic:
        # build executable
        build_cmd: chmod +x test*.sh
        # run simulation
        run_cmd: ./test.uniform.complex.sh
    toplevel: mux_fifo_tb

  # *** Test simple with osvvm

  simulate.run.osvvm.simple:
    default_tool: generic
    filesets : [core.rtl, simulate.rtl, simulate.extras]
    description : simulate with ghdl using own project makefile
    tools:
      generic:
        # build executable
        build_cmd: chmod +x test*.sh
        # run simulation
        run_cmd: ./test.osvvm.simple.sh
    toplevel: mux_fifo_tb

  # *** Test complex with osvvm

  simulate.run.osvvm.complex:
    default_tool: generic
    filesets : [core.rtl, simulate.rtl, simulate.extras]
    description : simulate with ghdl using own project makefile
    tools:
      generic:
        # build executable
        build_cmd: chmod +x test*.sh
        # run simulation
        run_cmd: ./test.osvvm.complex.sh
    toplevel: mux_fifo_tb

provider:
  name : git
  repo : https://gitlab.com/ip-vhdl/mux_fifo.git
